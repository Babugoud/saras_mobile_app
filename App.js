import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import SignInNavigator from './src/SignInNavigator';
import HomeNavigator from './src/HomeNavigator';

const RootNavigator = createStackNavigator(
  {
    // AuthCheck: {
    //   screen: AuthCheckScreen,
    //   navigationOptions: {
    //     header: null,
    //   },
    // },
    SignInNavigator: {
      screen: SignInNavigator,
      navigationOptions: {
        header: null,
      },
    },
    HomeScreenNavigator: {
      screen: HomeNavigator,
      navigationOptions: {
        header: null,
      },
    },
  },
  {initialRouteName: 'SignInNavigator'},
);

const AppMainNavigator = createAppContainer(RootNavigator);

export default function App() {
  return <AppMainNavigator />;
}
