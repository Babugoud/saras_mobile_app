import React from 'react';
import {StyleSheet, Text, View, StatusBar, Platform, Image} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Constants from '../constants';
const profileIcon = require('../Assets/Images/profile_icon.png');
export default function MyProfileScreen(props) {
  return (
    <View style={styles.container}>
      <View style={styles.userinfo}>
        <LinearGradient
          start={{x: 0.0, y: 1.0}}
          end={{x: 1.0, y: 1.0}}
          colors={['#2B2F2F', '#2B6A6A']}
          style={styles.gradient}>
          <View style={styles.statusBar}>
            <StatusBar
              barStyle="light-content"
              backgroundColor="transparent"
              translucent={true}
            />
          </View>
          <View>
            <Image source={profileIcon} style={styles.userIcon} />
            <Text style={[styles.userName, styles.appFontFamily]}>
              Sharath M S
            </Text>
          </View>
          <View style={styles.bottomLineStyle} />
        </LinearGradient>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  overlay: {
    opacity: 3,
  },
  bottomLineStyle: {
    height: 2.5,
    backgroundColor: 'white',
    top: 40,
  },
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 35,
  },
  header: {
    textAlign: 'center',
    flex: 1,
    color: '#FFFFFF',
    fontFamily: Constants.app_font_family_regular,
    fontSize: 17,
  },
  modalSpinnerStyle: {
    top: Constants.app_height / 2 - 50,
    height: 100,
    width: 100,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    borderRadius: 10,
  },
  spinnerStyle: {
    height: 20,
    width: 20,
    padding: 50,
    marginTop: 80,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  appFontFamily: {
    fontFamily: Constants.app_font_family_regular,
  },
  noNetwork: {
    bottom: 0,
    textAlign: 'center',
    color: 'white',
    paddingTop: 5,
    paddingBottom: 5,
    backgroundColor: 'grey',
    ...Platform.select({
      ios: {
        fontSize: 19,
      },
      android: {
        fontSize: 16,
      },
    }),
  },
  filterIconHolder: {
    flexDirection: 'row',

    width: 25,
    alignSelf: 'flex-end',
    ...Platform.select({
      android: {
        height: 145,
        marginTop: -50,
        marginRight: 10,
      },
      ios: {
        height: 170,
        marginTop: -20,
        marginRight: 15,
      },
    }),
  },
  userinfo: {
    width: '100%',
    height: 300,
  },
  navBar: {
    backgroundColor: '#FFFFFF',
    ...Platform.select({
      ios: {
        height: Constants.app_height >= 812 ? 88 : 73,
        shadowColor: 'rgba(0,0,0, .2)',
        shadowOffset: {height: 1, width: 0},
        shadowOpacity: 1,
        shadowRadius: 1,
      },
      android: {
        height: 55,
        borderBottomWidth: 1,
        borderBottomColor: '#D3D3D3',
      },
    }),
  },
  headerStyle: {
    flex: 1,
    textAlign: 'center',
    color: '#FFFFFF',
    fontFamily: Constants.app_font_family_regular,
    fontSize: 17,
    marginTop: 4,
  },

  gradient: {
    width: '100%',
    height: '100%',
  },
  bottomBorderLine1: {
    marginTop: 18,
    ...Platform.select({
      ios: {
        width: '36%',
        height: 2.5,
      },
      android: {
        width: '35.7%',
        height: 2.5,
      },
    }),
    backgroundColor: '#D3D3D3',
  },
  bottomBorderLine2: {
    marginTop: 16,
    ...Platform.select({
      ios: {
        width: '35.7%',
        height: 2.5,
        marginLeft: '28%',
      },
      android: {
        width: '35.7%',
        height: 2.5,
        marginLeft: '28.5%',
      },
    }),
    backgroundColor: '#D3D3D3',
  },
  userIcon: {
    top: 20,
    position: 'absolute',
    width: 100,
    height: 100,
    alignSelf: 'center',
    ...Platform.select({
      ios: {
        borderRadius: 50,
      },
      android: {
        borderRadius: 60,
      },
    }),
  },
  rewardsView: {
    alignSelf: 'center',
    textAlign: 'center',
    fontSize: 21,
    color: Constants.app_text_color,
  },
  userName: {
    alignSelf: 'center',
    textAlign: 'center',
    fontSize: 21,
    color: '#FFF',
    paddingTop: 125,
  },
  editHolder: {
    width: 80,
    height: 30,
    marginTop: 5,
    alignSelf: 'center',
    justifyContent: 'center',
    borderColor: '#C7C7C7',
    borderWidth: 1,
    borderRadius: 5,
  },
  edit: {
    fontSize: 15,
    textAlign: 'center',
    color: Constants.app_text_color,
  },
  mybadges: {
    justifyContent: 'center',
    color: '#434343',
    fontSize: 12,
    fontFamily: Constants.app_font_family,
    textAlign: 'center',
    alignItems: 'center',
    marginTop: 10,
  },
  bottomBorderLine: {
    borderBottomColor: Constants.app_dark_color,
    borderBottomWidth: 3,
    marginTop: 9,
    width: Constants.app_width / 3 - 20,
    marginLeft: 10,
  },
  certificateTitleHolder: {
    backgroundColor: '#f0f0f0',
    width: Constants.app_width / 3,
    height: 40,
    flexDirection: 'column',
    justifyContent: 'center',
    marginLeft: Constants.app_width / 3,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    elevation: 5,
  },
  certificateTitle: {
    color: '#434343',
    fontSize: 16,
    fontFamily: Constants.app_font_family,
    textAlign: 'center',
    alignItems: 'center',
    marginTop: '100',
  },
  certificatesListview: {
    flex: 1,
    marginTop: 10,
  },
  topViewText: {
    alignItems: 'center',
    alignSelf: 'center',
    fontSize: 10,
    marginTop: 0,
    color: Constants.app_text_color,
  },
  listcontainer: {
    width: '95%',
    flexDirection: 'row',
    alignItems: 'center',
    height: 80,
    margin: 7,
    borderRadius: 10,
    backgroundColor: 'white',
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0,0,0, .2)',
        shadowOffset: {height: 1, width: 0},
        shadowOpacity: 1,
        shadowRadius: 1,
      },
      android: {
        elevation: 1,
      },
    }),
  },
  badgeImage: {
    width: 35,
    height: 45,
    marginLeft: 15,
  },
  certDetailsHolder: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5,
    marginBottom: 5,
    ...Platform.select({
      ios: {
        width: Constants.app_width - 75,
      },
      android: {
        width: Constants.app_width - 70,
      },
    }),
  },
  topicName: {
    height: 45,
    fontSize: 16,
    color: Constants.app_text_color,
  },
  topicDate: {
    fontSize: 12,
    color: Constants.app_text_color,
    fontFamily: Constants.app_font_family_bold,
    ...Platform.select({
      ios: {
        fontWeight: 'bold',
      },
    }),
  },
  statusBar: {
    ...Platform.select({
      android: {
        height: 24,
      },
    }),
  },
  emptyListContainer: {
    justifyContent: 'center',
    flex: 1,
    margin: 10,
  },
});
