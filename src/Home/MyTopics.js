/* eslint-disable no-undef */
/* eslint-disable no-nested-ternary */
import React, {useEffect, useRef} from 'react';
import {
  StyleSheet,
  Image,
  Text,
  View,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  StatusBar,
  Platform,
} from 'react-native';

import {StackActions, NavigationActions} from 'react-navigation';
import Constants from '../constants';
import Toolbar from '../Toolbar';
import {FlatList} from 'react-native-gesture-handler';

const myTopicsWidth = (Constants.app_width - 10) / 2 - 10;
let myTopicsHeight = myTopicsWidth;
let progressViewHeight = 2;
if (myTopicsHeight / 2 < 80) {
  myTopicsHeight += 10;
}
if (myTopicsHeight <= 155) {
  myTopicsHeight += 10;
}
const myTopicsImgHeight = myTopicsHeight / 2;
const topicsWidth = (Constants.app_width - 5) / 3 - 5;
const topicsHeight = topicsWidth < 120 ? 120 : topicsWidth;
const topicsImgHeight = topicsHeight / 2;
const filter = require('../Assets/Images/filter_icon.png');

export default function MyTopicsScreen(props) {
  const {navigation} = props;

  function filterActiveTest() {}
  function filterUpcomingTest() {}
  function filterCompletedTest() {}
  function filterExpiredTest() {}
  return (
    <View style={styles.container}>
      <View style={styles.statusBar}>
        <StatusBar
          barStyle="light-content"
          backgroundColor={Constants.app_statusbar_color}
          translucent
        />
      </View>
      <Toolbar
        height={{height: 45}}
        center={<Text style={styles.headerStyle}>Computing and IT</Text>}
        right={
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity>
              <Image source={filter} style={styles.filterIcon} />
            </TouchableOpacity>
          </View>
        }
      />
      <ScrollView
        style={styles.scrollview}
        showsVerticalScrollIndicator={false}>
        <View>
          <View style={styles.filterViewHolder}>
            <TouchableHighlight
              onPress={() => {
                filterActiveTest();
              }}
              underlayColor="transparent">
              <View style={{flex: 1, flexDirection: 'column'}}>
                <Text style={[styles.topViewText1, styles.appFontFamily]}>
                  ACTIVE
                </Text>
              </View>
            </TouchableHighlight>
            <TouchableHighlight
              onPress={() => {
                filterUpcomingTest();
              }}
              underlayColor="transparent">
              <View style={{flex: 1, flexDirection: 'column'}}>
                <Text style={[styles.topViewText1, styles.appFontFamily]}>
                  UPCOMING
                </Text>
              </View>
            </TouchableHighlight>
            <TouchableHighlight
              onPress={() => {
                filterCompletedTest();
              }}
              underlayColor="transparent">
              <View style={{flex: 1, flexDirection: 'column'}}>
                <Text style={[styles.topViewText1, styles.appFontFamily]}>
                  COMPLETED
                </Text>
              </View>
            </TouchableHighlight>
            <TouchableHighlight
              onPress={() => {
                filterExpiredTest();
              }}
              underlayColor="transparent">
              <View style={{flex: 1, flexDirection: 'column'}}>
                <Text style={[styles.topViewText1, styles.appFontFamily]}>
                  EXPIRED
                </Text>
              </View>
            </TouchableHighlight>
          </View>
        </View>
        <FlatList
          style={styles.mytopicsListview}
          numColumns={2}
          showsHorizontalScrollIndicator={false}
        />
        <View style={styles.mytopicsRowContainer}>
          <Text style={[styles.myTopicTitle, styles.appFontFamily]}>
            Demo Test - 02
          </Text>
          <View style={styles.topLineStyle} />
          <View>
            <TouchableHighlight
              style={[styles.notAttemptedContainer, styles.notAttemptedButton]}>
              <Text style={styles.notAttemptText}>NOT ATTEMPTED</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.bottomLineStyle} />
          <View>
            <TouchableHighlight
              style={[styles.buttonContainer, styles.loginButton]}
              onPress={() => navigation.navigate('TestDetails')}>
              <Text style={styles.loginText}>Start</Text>
            </TouchableHighlight>
          </View>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  rewardsView: {
    alignSelf: 'center',
    textAlign: 'center',
    fontSize: 18,
    color: Constants.app_text_color,
  },
  statusBar: {
    ...Platform.select({
      android: {
        height: 24,
      },
    }),
  },
  progressHeader: {
    width: '90%',
    height: 80,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#f0f0f0',
    borderRadius: 10,
    marginLeft: 18.5,
    borderColor: '#707070',
    borderWidth: 0,
    marginTop: 15,
    ...Platform.select({
      ios: {
        shadowOffset: {width: 1, height: 1},
        elevation: 1,
        shadowOpacity: 0.5,
        shadowRadius: 1,
      },
      android: {
        shadowOffset: {width: 2, height: 2},
        elevation: 3,
        shadowOpacity: 1,
        shadowRadius: 2,
      },
    }),
  },
  bottomLineStyle: {
    top: 120,
    height: 0.5,
    backgroundColor: Constants.app_grey_color,
  },
  topLineStyle: {
    top: 120,
    height: 0.5,
    backgroundColor: Constants.app_grey_color,
  },
  progressTab: {
    width: 100,
    height: 80,
    marginTop: 10,
  },
  progressTab1: {
    width: 100,
    height: 80,
    marginTop: 8,
  },
  progressBarLine: {
    borderBottomColor: '#D3D3D3',
    borderBottomWidth: 1,
    marginTop: 15,
  },
  headerStyle: {
    flex: 1,
    textAlign: 'center',
    color: '#FFFFFF',
    fontFamily: Constants.app_font_family_regular,
    fontSize: 17,
    marginTop: 4,
  },
  center: {
    flexDirection: 'row',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  dividerLine: {
    borderLeftWidth: 0.5,
    borderLeftColor: '#707070',
    height: 60,
    marginTop: 10,
    marginBottom: 10,
    width: 2,
  },
  progressImage: {
    justifyContent: 'center',
    width: 15,
    height: 15,
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 0,
  },
  certificateImage: {
    justifyContent: 'center',
    width: 20,
    height: 20,
    alignItems: 'center',
    alignSelf: 'center',
  },
  dueDate: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    zIndex: 999,
    backgroundColor: '#db5b5b',
    position: 'absolute',
    top: 0,
    width: myTopicsWidth,
    height: 20,
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: Constants.app_background_color,
  },
  right: {
    justifyContent: 'flex-end',
    marginRight: 20,
  },
  noNetwork: {
    bottom: 0,
    textAlign: 'center',
    color: 'white',
    paddingTop: 5,
    paddingBottom: 5,
    backgroundColor: 'grey',
    ...Platform.select({
      ios: {
        fontSize: 19,
      },
      android: {
        fontSize: 16,
      },
    }),
  },
  scrollview: {
    ...Platform.select({
      ios: {
        marginTop: 1,
      },
      android: {
        marginTop: 0,
      },
    }),
  },
  sectionHeader: {
    width: Constants.app_width,
    height: 40,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
  appFontFamily: {
    fontFamily: Constants.app_font_family_regular,
  },
  topViewText: {
    alignItems: 'center',
    alignSelf: 'center',
    fontSize: 9,
    marginTop: 13,
    color: '#000000',
  },
  topViewText1: {
    alignItems: 'center',
    alignSelf: 'center',
    fontSize: 12,
    marginTop: 13,
    color: '#000000',
  },
  headerText1: {
    ...Platform.select({
      ios: {
        fontSize: 14,
      },
      android: {
        marginTop: 0,
        fontSize: 12,
        justifyContent: 'center',
        alignSelf: 'center',
      },
    }),
    color: '#FFFFFF',
  },
  headerText: {
    ...Platform.select({
      ios: {
        fontSize: 19,
      },
      android: {
        fontSize: 16,
      },
    }),
    color: Constants.app_text_color,
  },
  mytopicsListview: {
    flex: 1,
    paddingLeft: 5,
    paddingRight: 5,
    paddingBottom: 5,
    marginTop: 8,
  },
  topicsListview: {
    flex: 1,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 8,
    marginBottom: 5,
  },
  mytopicsRowContainer: {
    flexDirection: 'column',
    width: myTopicsWidth,
    height: myTopicsHeight + 90,
    margin: 5,
    borderRadius: 10,
    backgroundColor: 'white',
    borderColor: 'black',
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0,0,0, .2)',
        shadowOffset: {height: 1, width: 0},
        shadowOpacity: 1,
        shadowRadius: 1,
      },
      android: {
        elevation: 1,
      },
    }),
  },
  topTopicsRowContainer: {
    width: topicsWidth,
    height: topicsHeight - 15,
    margin: 2,
    borderRadius: 10,
    backgroundColor: 'white',
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0,0,0, .2)',
        shadowOffset: {height: 1, width: 0},
        shadowOpacity: 1,
        shadowRadius: 1,
      },
      android: {
        shadowOpacity: 1,
        elevation: 1,
      },
    }),
  },
  myTopicImg: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    width: myTopicsWidth,
    height: myTopicsImgHeight,
    ...Platform.select({
      ios: {
        overflow: 'hidden',
      },
    }),
  },
  topicImg: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    width: topicsWidth,
    height: topicsImgHeight,
    ...Platform.select({
      ios: {
        overflow: 'hidden',
      },
    }),
  },
  myTopicTitle: {
    height: 50,
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
    ...Platform.select({
      ios: {
        fontSize: 15,
      },
      android: {
        fontSize: 13,
      },
    }),
    textAlign: 'center',
    justifyContent: 'center',
    color: 'white',
    backgroundColor: '#00b5ec',
  },
  topicTitle: {
    margin: 5,
    textAlign: 'center',
    ...Platform.select({
      ios: {
        fontSize: 13,
      },
      android: {
        fontSize: 11,
      },
    }),
    color: Constants.app_text_color,
  },
  topicDetails: {
    height: 20,
    marginLeft: 20,
    marginRight: 20,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  nuggets: {
    textAlign: 'left',
    alignSelf: 'center',
    fontSize: 12,
    color: Constants.app_text_color,
  },
  emptyMsgText: {
    height: 70,
    marginTop: 80,
    color: '#484848',
    ...Platform.select({
      ios: {
        fontSize: 15,
      },
      android: {
        fontSize: 16,
      },
    }),
    fontFamily: Constants.app_font_family_regular,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  progressView: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 15,
    borderRadius: 20,
    overflow: 'hidden',
  },
  andoidProgressView: {
    height: progressViewHeight,
  },
  emptyMyTopicsContainer: {
    flex: 1,
    height: 180,
    marginLeft: 5,
    marginTop: 8,
    marginRight: 5,
    borderRadius: 10,
    backgroundColor: 'white',
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
    opacity: 0.2,
  },
  titleHolder: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  filterViewHolder: {
    width: '95%',
    height: 40,
    paddingLeft: 5,
    paddingRight: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    // borderBottomRightRadius: 10,
    // borderBottomLeftRadius: 10,
    marginLeft: 10.5,
    borderColor: '#f0f0f0',
    marginTop: 0,
    ...Platform.select({
      ios: {
        shadowOffset: {width: 1, height: 1},
        elevation: 1,
        shadowOpacity: 0.5,
        shadowRadius: 1,
      },
      android: {
        shadowOffset: {width: 2, height: 2},
        elevation: 3,
        shadowOpacity: 1,
        shadowRadius: 2,
      },
    }),
  },
  listContainer: {
    flex: 1,
    width: '100%',
    borderBottomWidth: 1,
    flexDirection: 'row',
    borderBottomColor: Constants.app_grey_color,
    alignItems: 'center',
    alignSelf: 'center',
  },
  nextButton: {
    width: 22,
    height: 22,
    tintColor: '#FFFFFF',
    marginTop: 4,
    marginRight: 5,
  },
  eventInnerHolder: {
    width: '22.5%',
    height: '100%',
    borderRightWidth: 1,
    borderRightColor: Constants.app_grey_color,
    paddingRight: 5,
    paddingBottom: 5,
  },
  eventTextHolder: {
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 5,
  },
  eventDateText: {
    fontSize: 40,
    padding: 0,
    marginLeft: -10,
    color: '#606061',
  },
  eventMonthYearHolder: {
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: 5,
  },
  eventMonthYearText: {
    fontSize: 16,
    padding: 0,
    color: '#606061',
  },
  eventLinkHolder: {
    width: '77.5%',
    paddingHorizontal: 10,
    paddingVertical: 10,
    color: '#606061',
  },
  eventLinkText: {
    marginTop: 10,
    color: '#3366BB',
    textDecorationLine: 'underline',
  },
  yesNotStarted: {
    backgroundColor: '#f0f0f0',
    width: Constants.app_width / 3 - 12,
    height: 40,
    borderBottomLeftRadius: 10,
    borderBottomColor: Constants.app_dark_color,
    borderBottomWidth: 2,
  },
  noNotStarted: {
    backgroundColor: '#FFFFFF',
    width: Constants.app_width / 3 - 12,
    height: 40,
    borderBottomLeftRadius: 10,
  },
  yesInProgress: {
    width: Constants.app_width / 3 - 12,
    height: 40,
    backgroundColor: '#f0f0f0',
    borderBottomColor: Constants.app_dark_color,
    borderBottomWidth: 2,
  },
  noInProgress: {
    width: Constants.app_width / 3 - 12,
    height: 40,
    backgroundColor: '#FFFFFF',
  },
  yesCompleted: {
    width: Constants.app_width / 3 - 12,
    height: 40,
    marginRight: 30,
    backgroundColor: '#f0f0f0',
    borderBottomRightRadius: 10,
    borderBottomColor: Constants.app_dark_color,
    borderBottomWidth: 2,
  },
  noCompleted: {
    width: Constants.app_width / 3 - 12,
    height: 40,
    marginRight: 30,
    backgroundColor: '#FFFFFF',
    borderBottomRightRadius: 10,
  },
  eventTouchForNextImage: {
    position: 'absolute',
    right: 10,
    width: 30,
    height: 30,
  },
  filterIcon: {
    height: 24,
    width: 18,
    marginTop: 9,
    marginHorizontal: 10,
  },
  notifyImage: {
    height: 22,
    width: 18,
    marginTop: 9,
  },
  loadingEventsText: {
    fontSize: 16,
    color: 'grey',
    paddingTop: 20,
    paddingBottom: 20,
  },
  noEvents: {
    fontSize: 16,
    color: 'grey',
    paddingTop: 20,
    paddingBottom: 20,
  },
  loadingEventsView: {
    flex: 1,
    alignItems: 'center',
    alignSelf: 'center',
  },
  noUpcomingEventsView: {
    flex: 1,
    alignItems: 'center',
    alignSelf: 'center',
  },
  spinnerView: {
    flex: 1,
    position: 'absolute',
    bottom: 0,
    width: '100%',
    opacity: 0.8,
    zIndex: 1000,
  },
  borderOne: {
    borderBottomColor: '#D3D3D3',
    borderBottomWidth: 1,
    marginTop: 8,
  },
  borderTwo: {
    borderBottomColor: '#D3D3D3',
    borderBottomWidth: 1,
    marginTop: 10,
  },
  buttonContainer: {
    top: 130,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: 0,
    padding: 0,
    borderRadius: 5,
  },
  notAttemptedContainer: {
    top: 120,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: 0,
    padding: 0,
  },
  loginButton: {
    marginRight: 5,
    width: '50%',
    height: 30,
    marginBottom: 0,
    padding: 0,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    backgroundColor: '#00b5ec',
  },
  notAttemptedButton: {
    marginRight: 5,
    marginLeft: 5,
    width: '100%',
    height: 30,
    marginBottom: 0,
    padding: 0,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    backgroundColor: 'white',
  },
  loginText: {
    color: 'white',
    fontSize: 14,
    fontFamily: Constants.app_font_family_regular,
    ...Platform.select({
      ios: {
        fontWeight: 'normal',
      },
    }),
    ...Platform.select({
      android: {
        justifyContent: 'center',
        marginBottom: 1.5,
      },
    }),
  },
  notAttemptText: {
    color: Constants.app_grey_color,
    fontSize: 14,
    fontFamily: Constants.app_font_family_regular,
    ...Platform.select({
      ios: {
        fontWeight: 'normal',
      },
    }),
    ...Platform.select({
      android: {
        justifyContent: 'center',
        marginBottom: 1.5,
      },
    }),
  },
});
