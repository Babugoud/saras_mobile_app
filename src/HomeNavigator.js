import React from 'react';
import {Image, View} from 'react-native';
import {createAppContainer} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';
import getSlideFromRightTransition from 'react-navigation-slide-from-right-transition';
import Constants from './constants';
import TestDetailsScreen from './Topics/TestDetails';
import TestStartDetailsScreen from './Topics/TestStartDetails';
import MyTopicsScreen from './Home/MyTopics';
import MyProfileScreen from './Home/MyProfile';

const homeSelected = require('./Assets/Images/home_selected.png');
const homeNotSelected = require('./Assets/Images/home_notselected.png');
const myProfileSelected = require('./Assets/Images/myprofile_selected.png');
const myProfileNotSelected = require('./Assets/Images/myprofile_notselected.png');

const MyTopicsStack = createStackNavigator(
  {
    MyTopics: {
      screen: MyTopicsScreen,
      navigationOptions: {
        header: null,
        headerBackTitle: null,
      },
    },
  },
  {transitionConfig: getSlideFromRightTransition},
);

const MyProfileStack = createStackNavigator(
  {
    MyProfile: {
      screen: MyProfileScreen,
      navigationOptions: {
        header: null,
        headerBackTitle: null,
      },
    },
  },
  {transitionConfig: getSlideFromRightTransition},
);

const TabNavigator = createBottomTabNavigator(
  {
    MyTopics: {
      screen: MyTopicsStack,
      title: 'Home',
      navigationOptions: {
        tabBarLabel: <View Home />,
        tabBarIcon: ({tintColor, focused}) =>
          focused ? (
            <Image
              source={homeSelected}
              style={{height: 22, width: 22, tintColor}}
            />
          ) : (
            <Image
              source={homeNotSelected}
              style={{height: 22, width: 22, opacity: 0.5}}
            />
          ),
      },
    },
    MyProfile: {
      screen: MyProfileStack,
      title: 'My Profile',
      navigationOptions: {
        tabBarLabel: <View My Profile />,
        tabBarIcon: ({tintColor, focused}) =>
          focused ? (
            <Image
              source={myProfileSelected}
              style={{height: 24, width: 24, tintColor}}
            />
          ) : (
            <Image
              source={myProfileNotSelected}
              style={{height: 24, width: 24, opacity: 0.5}}
            />
          ),
      },
    },
  },
  {
    transitionConfig: getSlideFromRightTransition,
    tabBarOptions: {
      activeTintColor: Constants.app_button_color,
      inactiveTintColor: 'grey',
      inactiveBackgroundColor: '#ffffff',
      activeBackgroundColor: '#ffffff',
      showIcon: true,
      scrollEnabled: true,
      indicatorStyle: {
        borderBottomColor: '#ffffff',
        borderBottomWidth: 0,
      },
      iconStyle: {},
      labelStyle: {
        fontSize: 11,
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: Constants.app_font_family_regular,
      },
      style: {
        backgroundColor: '#ffffff',
        height: 60,
      },
      tabStyle: {
        paddingTop: 9,
        paddingBottom: 9,
      },
    },
  },
);

const HomeNavigator = createStackNavigator(
  {
    Tab: {
      screen: TabNavigator,
      navigationOptions: {
        header: null,
        headerLeft: null,
        headerBackTitle: null,
        headerStyle: {
          backgroundColor: Constants.app_color,
          elevation: 0,
          shadowOpacity: 0,
        },
        headerTintColor: '#ffffff',
        headerTitleStyle: {
          textAlign: 'center',
          flex: 1,
          color: '#ffffff',
          fontSize: 19,
          fontFamily: Constants.app_font_family_regular,
        },
      },
    },
    TestDetails: {
      screen: TestDetailsScreen,
      navigationOptions: {
        gesturesEnabled: false,
        header: null,
        headerBackTitle: null,
      },
    },
    TestStartDetails: {
      screen: TestStartDetailsScreen,
      navigationOptions: {
        gesturesEnabled: false,
        header: null,
        headerBackTitle: null,
      },
    },
    // ObjectDetails: {
    //   screen: ObjectDetailsScreen,
    //   navigationOptions: {
    //     gesturesEnabled: false,
    //     header: null,
    //     headerBackTitle: null,
    //   },
    // },
    // BookmarkList: {
    //   screen: BookmarkListScreen,
    //   navigationOptions: {
    //     gesturesEnabled: false,
    //     header: null,
    //     headerBackTitle: null,
    //   },
    // },
    // NuggetDetails: {
    //   screen: NuggetDetailsScreen,
    //   navigationOptions: {
    //     gesturesEnabled: false,
    //     header: null,
    //     headerBackTitle: null,
    //   },
    // },
    // QuizDetails: {
    //   screen: QuizDetailsScreen,
    //   navigationOptions: {
    //     gesturesEnabled: false,
    //     headerBackTitle: null,
    //     header: null,
    //   },
    // },
    // QuizObject: {
    //   screen: QuizObjectScreen,
    //   navigationOptions: {
    //     gesturesEnabled: false,
    //     headerBackTitle: null,
    //     header: null,
    //   },
    // },
    // TopicList: {
    //   screen: TopicListScreen,
    //   navigationOptions: {
    //     gesturesEnabled: false,
    //     header: null,
    //     headerBackTitle: null,
    //   },
    // },
    // ViewCertificate: {
    //   screen: ViewCertificateScreen,
    //   navigationOptions: {
    //     gesturesEnabled: false,
    //     header: null,
    //     headerBackTitle: null,
    //   },
    // },
    // Settings: {
    //   screen: SettingsScreen,
    //   navigationOptions: {
    //     header: null,
    //     headerBackTitle: null,
    //   },
    // },
    // MultimediaView: {
    //   screen: MultimediaViewScreen,
    //   navigationOptions: {
    //     header: null,
    //     headerBackTitle: null,
    //   },
    // },

    // Notifications: {
    //   screen: NotificationsScreen,
    //   navigationOptions: {
    //     header: null,
    //     headerBackTitle: null,
    //   },
    // },
    // MyProgress: {
    //   screen: MyProgressScreen,
    //   navigationOptions: {
    //     header: null,
    //     headerBackTitle: null,
    //   },
    // },
  },
  {transitionConfig: getSlideFromRightTransition},
);

export default createAppContainer(HomeNavigator);
