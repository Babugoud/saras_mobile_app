/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useRef, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Image,
  TouchableWithoutFeedback,
  Keyboard,
  Platform,
  StatusBar,
  Modal,
  BackHandler,
  ActivityIndicator,
} from 'react-native';
import Constants from '../constants';
import {StackActions, NavigationActions} from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import SInfo from 'react-native-sensitive-info';
import Config from 'react-native-config';
const logoWidth = 210;
const logoHeight = 50;
const smallIcon = 20;
const mainLogo = require('../Assets/Images/mainlogo.png');
const loginName = require('../Assets/Images/mail.png');
const password = require('../Assets/Images/password.png');

export default function LoginScreen(props) {
  const [states, setStates] = useState({
    LoginName: '',
    Key: '',
    spinner: false,
    status: false,
    validation: '',
  });
  const [isLoading, setIsLoading] = useState(true);
  const focusHere = useRef(null);
  const {navigation} = props;

  useEffect(() => {
    getApiToken();
    signIn();
    BackHandler.addEventListener('hardwareBackPress', handleBackButton);
    LoginScreen.navListener = navigation.addListener('didFocus', () => {
      StatusBar.setBarStyle('light-content');
      if (Platform.OS === 'android') {
        StatusBar.setBackgroundColor('transparent');
        StatusBar.setTranslucent(true);
      }
    });
    return () => {
      removeBackPressListner();
      LoginScreen.navListener.remove();
    };
  }, []);

  function handleBackButton() {
    return false;
  }

  function removeBackPressListner() {
    BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
  }
  function onChangeEmailText(value) {
    setStates({
      ...states,
      status: false,
      validation: '',
      LoginName: value,
    });
  }

  function onChangePasswordText(value) {
    setStates({
      ...states,
      status: false,
      validation: '',
      Key: value,
    });
  }
  async function storeData() {
  try {
    await AsyncStorage.setItem('@storage_Key', 'stored value')
  } catch (e) {
    // saving error
  }
}

  async function loginButtonPressed() {
    // const reg = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
    if (states.LoginName.trim() === '') {
      setStates({
        ...states,
        validation: 'Please enter Login name',
        status: true,
      });
      return;
    }
    if (states.Key.trim() === '') {
      setStates({
        ...states,
        validation: 'Please enter Password',
        status: true,
      });
      return;
    }
    setStates({
      ...states,
      validation: null,
      status: false,
      spinner: true,
    });
    signIn(states.LoginName, states.Key);
  }
  function getApiToken() {
    let formData = new FormData();
      formData.append('grant_type', 'password');
      formData.append('username', 'ReactNUser@enhanzed.com');
      formData.append('password', 'Re@c1nUser');
        fetch('https://eassessmentbuild.excelindia.com/TNA/SARASAPI/Token', {
          method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/x-www-form-urlencoded',
          },
            body: formData
    })
      .then((response) => response.json())
      .then((responseData) => {
          alert(JSON.stringify(responseJson));
          console.log("Res=="+responseData);
    });
  }
  async function signIn() {
   const yourBearerToken = 'b57D_3eVieMnRoaA9nhF8Q1l1fF6ZLD6cUiTu3cdlQaF0zjB1wiiRfNCv0LtoAWz096jgjlaLgRM6HcapCoA5hGyouIW_FPsfNMwiIIYMSG7Y1vZyX79HNEPwRafOOAor6hkQcAbKNmYXvweDuwUNAdiVhO7F8NLhy7UgIHgskWY56_zHHZPHZHistiCU9hgC8Tbw134hmR550_7uwsEJa49D5Gh3kpyOVnhtQwxCw242QU_jurmKn8n91UM8Wjq1YgZ-La3h0XgK3MbytgmSspI_wXjBaGn0l0S1bSMiN-VkIBbxQcBS5grljQkwc6ckT68Y-F5NQY4anaEJazjBw9_ktNxHu5Xb0NfLVXS-wFF5dSuk88jXMJrsO9vP4YF0mmpPkCIMNTn58oVadI7XqjgFLPtSeUYy7dFYpL0V3dpVm3d0XbOpGqQ_310ukiP7XlQjun3o8fniDQHeEX86VB2ccRtpFbe8PIOOGQoSgPop8EBn_LHY5Dq7LQoRwsP'
    fetch('https://eassessmentbuild.excelindia.com/TNA/SARASAPI/Authentication/CheckUser', {
       method: 'POST',
      headers: {
        'Cache-Control': 'no-cache',
         Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + yourBearerToken
      },
      body: JSON.stringify({
             'LoginName': 'ESWilliam',
             'Key': 'password'
          }),
})
    .then((response) => response.json())
    //If response is in json then in success
    .then(async(response) => {
            // setIsLoading(false);
            if(response.status == "success"){
                navigate('HomeScreenNavigator');
                await AsyncStorage.setItem('IsLoggedIn', 'true');
            }else{
                console.log("Info=="+JSON.stringify(response))
            }
        })
        .catch((error) => {
            console.log("Res=="+error);
        });
    }
  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
      <View style={styles.container}>
        <View style={styles.statusBar}>
          <StatusBar
            barStyle="light-content"
            backgroundColor="transparent"
            translucent
          />
        </View>
        <Modal animationType="none" transparent visible={states.spinner}>
          <View style={styles.spinnerStyle}>
            <ActivityIndicator
              animating
              size="large"
              color={Constants.app_button_color}
            />
          </View>
        </Modal>
        <View style={styles.logoHolder}>
          <Image source={mainLogo} style={styles.logoStyle} />
        </View>
        <View style={styles.holderView}>
          <View style={{marginLeft: 15, marginRight: 15, marginBottom: 0}}>
            {states.status ? (
              <Text style={styles.validationText}>{states.validation}</Text>
            ) : null}
          </View>
          <View style={styles.textfieldHolder}>
            <View style={styles.textfieldStyle}>
              <Image source={loginName} style={styles.emailIconStyle} />
              <TextInput
                underlineColorAndroid="transparent"
                style={styles.input}
                returnKeyType="next"
                keyboardType="email-address"
                placeholder="Login name"
                placeholderTextColor="#c7c7c7"
                blurOnSubmit={false}
                autoCapitalize="none"
                onSubmitEditing={() => focusHere.current.focus()}
                onChangeText={val => onChangeEmailText(val)}
              />
            </View>
            <View style={styles.bottomLineStyle} />
          </View>
          <View style={[styles.textfieldHolder, {marginTop: 10}]}>
            <View style={styles.textfieldStyle}>
              <Image source={password} style={styles.passwordIconStyle} />
              <TextInput
                underlineColorAndroid="transparent"
                ref={focusHere}
                style={styles.passwordInput}
                returnKeyType="done"
                placeholder="Password"
                placeholderTextColor="#c7c7c7"
                secureTextEntry
                onSubmitEditing={() => loginButtonPressed()}
                onChangeText={val => onChangePasswordText(val)}
              />
            </View>
            <View style={styles.bottomLineStyle} />
          </View>

          <TouchableHighlight
            style={[styles.buttonContainer, styles.loginButton]}
            onPress={() => loginButtonPressed()}>
            <Text style={styles.loginText}>Login</Text>
          </TouchableHighlight>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  holderView: {
    position: 'absolute',
    top: Constants.app_height / 2 - 120,
    width: '100%',
    height: Constants.app_height / 2 + 50,
    backgroundColor: 'transparent',
  },
  spinnerStyle: {
    top: Constants.app_height / 2 - 50,
    height: 100,
    width: 100,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    borderRadius: 10,
  },
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#FFFFFF',
    borderRadius: 30,
    borderBottomWidth: 1,
    width: 250,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  statusBar: {
    ...Platform.select({
      android: {
        height: 24,
      },
    }),
  },
  validationText: {
    marginTop: -20,
    marginLeft: 63,
    position: 'absolute',
    color: 'red',
    fontSize: 11,
    fontFamily: Constants.app_font_family_regular,
  },
  passwordInput: {
    flex: 1,
    marginLeft: 2.9,
    marginTop: 9,
    color: 'black',
    ...Platform.select({
      android: {
        fontSize: 14,
      },
      ios: {
        fontSize: 16,
      },
    }),
    fontFamily: Constants.app_font_family_regular,
    padding: 0,
  },
  textfieldHolder: {
    height: 40,
    marginTop: 0,
    marginBottom: 10,
    marginLeft: 35,
    marginRight: 35,
    flexDirection: 'column',
    backgroundColor: 'transparent',
  },
  passwordIconStyle: {
    marginLeft: 10,
    marginRight: 11,
    marginTop: 10,
    width: 15,
    height: smallIcon,
  },
  input: {
    flex: 1,
    marginLeft: 3,
    marginTop: 9,
    color: 'black',
    ...Platform.select({
      android: {
        fontSize: 14,
      },
      ios: {
        fontSize: 16,
      },
    }),
    fontFamily: Constants.app_font_family_regular,
    padding: 0,
  },
  emailIconStyle: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 13,
    width: 17,
    height: 17,
  },
  textfieldStyle: {
    height: 30,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  bottomLineStyle: {
    height: 0.5,
    backgroundColor: Constants.app_grey_color,
  },
  logoStyle: {
    width: logoWidth,
    height: logoHeight,
  },
  logoHolder: {
    marginTop: -15,
    marginBottom: 0,
    position: 'absolute',
    alignSelf: 'center',
    top: 100,
  },
  buttonContainer: {
    top: 80,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: 0,
    padding: 0,
  },
  loginButton: {
    marginRight: 5,
    width: '30%',
    height: 30,
    marginBottom: 0,
    padding: 0,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    backgroundColor: Constants.app_button_color,
  },
  loginText: {
    color: 'white',
    fontSize: 14,
    fontFamily: Constants.app_font_family_regular,
    ...Platform.select({
      ios: {
        fontWeight: 'normal',
      },
    }),
    ...Platform.select({
      android: {
        justifyContent: 'center',
        marginBottom: 1.5,
      },
    }),
  },
});
