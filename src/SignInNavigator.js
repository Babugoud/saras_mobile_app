import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import getSlideFromRightTransition from 'react-navigation-slide-from-right-transition';

import LoginScreen from './Login/Login';
import './constants';

const SignInNavigator = createStackNavigator(
  {
    Login: {
      screen: LoginScreen,
      navigationOptions: {
        header: null,
        headerBackTitle: null,
      },
    },
  },
  {initialRouteName: 'Login', transitionConfig: getSlideFromRightTransition},
);

export default createAppContainer(SignInNavigator);
