import React from 'react';
import {StyleSheet, View} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';
// import {isIphoneX} from '../Home/isIphoneX';
import Constants from './constants';

export default function Toolbar(props) {
  const {
    left,
    center,
    right,
    bgColor = [Constants.app_toolbar_color, Constants.app_toolbar_color],
    height,
  } = props;

  return (
    <LinearGradient colors={bgColor} style={styles.linearGradient}>
      <View style={height}>
        <View style={styles.left}>{left}</View>
        <View style={styles.center}>{center}</View>
        <View style={styles.right}>{right}</View>
      </View>
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  // container: {

  //   flexDirection: 'row',
  //   justifyContent: 'space-between',
  //   alignItems: 'center',
  //   shadowColor: 'rgba(114, 133, 168, 0.17)',
  //   shadowOffset: {
  //     width: 0,
  //     height: 2,
  //   },
  //   shadowRadius: 3,
  //   shadowOpacity: 1,
  //   elevation: 2,
  // },
  // linearGradient: {
  //   paddingTop: isIphoneX(),
  // },
  left: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'flex-start',
    marginLeft: 20,
  },
  right: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'flex-end',
    marginRight: 20,
  },
  center: {
    flex: 1,
    justifyContent: 'center',
    marginHorizontal: 50,
    alignSelf: 'center',
    marginTop: 5,
    position: 'absolute',
  },
});
