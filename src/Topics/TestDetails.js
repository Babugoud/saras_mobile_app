import React, {useEffect, useRef} from 'react';
import {
  StyleSheet,
  Image,
  Text,
  View,
  TouchableHighlight,
  TouchableOpacity,
  StatusBar,
  Platform,
} from 'react-native';

import Constants from '../constants';
import Toolbar from '../Toolbar';
import {StackActions, NavigationActions} from 'react-navigation';
const myTopicsWidth = (Constants.app_width - 10) / 2 - 10;
let myTopicsHeight = myTopicsWidth;

if (myTopicsHeight / 2 < 80) {
  myTopicsHeight += 10;
}
if (myTopicsHeight <= 155) {
  myTopicsHeight += 10;
}

const backIcon = require('../Assets/Images/back_icon.png');

export default function TestDetailsSceen(props) {
  const {navigation} = props;
  return (
    <View style={styles.container}>
      <View style={styles.statusBar}>
        <StatusBar
          barStyle="light-content"
          backgroundColor={Constants.app_statusbar_color}
          translucent
        />
      </View>
      <Toolbar
        height={{height: 45}}
        center={<Text style={styles.headerStyle}>Demo Test - 02</Text>}
        left={
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity onPress={() => navigation.navigate('MyTopics')}>
              <Image source={backIcon} style={styles.backIcon} />
            </TouchableOpacity>
          </View>
        }
      />
      {/* <ScrollView
        style={styles.scrollview}
        showsVerticalScrollIndicator={false}> */}
      <View>
        <TouchableHighlight
          onPress={() => navigation.navigate('TestStartDetails')}
          style={[styles.buttonContainer, styles.startButton]}>
          <Text style={styles.startText}>Start</Text>
        </TouchableHighlight>
      </View>
      {/* </ScrollView> */}
    </View>
  );
}

const styles = StyleSheet.create({
  statusBar: {
    ...Platform.select({
      android: {
        height: 24,
      },
    }),
  },
  headerStyle: {
    flex: 1,
    textAlign: 'center',
    color: '#FFFFFF',
    fontFamily: Constants.app_font_family_regular,
    fontSize: 17,
    marginTop: 4,
  },
  center: {
    flexDirection: 'row',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: Constants.app_background_color,
  },
  right: {
    justifyContent: 'flex-end',
    marginRight: 20,
  },
  noNetwork: {
    bottom: 0,
    textAlign: 'center',
    color: 'white',
    paddingTop: 5,
    paddingBottom: 5,
    backgroundColor: 'grey',
    ...Platform.select({
      ios: {
        fontSize: 19,
      },
      android: {
        fontSize: 16,
      },
    }),
  },
  scrollview: {
    ...Platform.select({
      ios: {
        marginTop: 1,
      },
      android: {
        marginTop: 0,
      },
    }),
  },
  appFontFamily: {
    fontFamily: Constants.app_font_family_regular,
  },
  backIcon: {
    height: 24,
    marginLeft: 5,
    width: 20,
    marginTop: 9,
    marginHorizontal: 10,
  },
  buttonContainer: {
    top: Constants.app_height - 100,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: 0,
    padding: 0,
  },
  startButton: {
    width: '100%',
    height: 50,
    marginBottom: 0,
    padding: 0,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    backgroundColor: '#00b5ec',
  },
  startText: {
    color: 'white',
    fontSize: 18,
    fontFamily: Constants.app_font_family_regular,
    ...Platform.select({
      ios: {
        fontWeight: 'normal',
      },
    }),
    ...Platform.select({
      android: {
        justifyContent: 'center',
        marginBottom: 1.5,
      },
    }),
  },
});
