import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Image,
  Text,
  View,
  TouchableHighlight,
  TouchableOpacity,
  StatusBar,
  Platform,
  Button,
  ScrollView,
  Dimensions,
} from 'react-native';

import Constants from '../constants';
import Toolbar from '../Toolbar';
import {StackActions, NavigationActions} from 'react-navigation';
const myTopicsWidth = (Constants.app_width - 10) / 2 - 10;
let myTopicsHeight = myTopicsWidth;

if (myTopicsHeight / 2 < 80) {
  myTopicsHeight += 10;
}
if (myTopicsHeight <= 155) {
  myTopicsHeight += 10;
}

const backIcon = require('../Assets/Images/back_icon.png');
var screenWidth = Dimensions.get('window').width;
export default function TestStartDetailsSceen(props) {
  const {navigation} = props;
  return (
    <View style={styles.container}>
      <View style={styles.statusBar}>
        <StatusBar
          barStyle="light-content"
          backgroundColor={Constants.app_statusbar_color}
          translucent
        />
      </View>
      <Toolbar
        height={{height: 45}}
        center={<Text style={styles.headerStyle}>Demo Test - 02</Text>}
        left={
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity
              onPress={() => navigation.navigate('TestDetails')}>
              <Image source={backIcon} style={styles.backIcon} />
            </TouchableOpacity>
          </View>
        }
      />
      <View>
        <TouchableHighlight
          style={[styles.submitContainer, styles.submitButton]}>
          <Text style={styles.submitText}>SUBMIT</Text>
        </TouchableHighlight>
      </View>
      <View style={styles.bottomLineStyle} />
      {/* <ScrollView
        style={styles.scrollview}
        showsVerticalScrollIndicator={false}> */}
      <View style={styles.MainContainer}>
        <ScrollView
          horizontal={true}
          pagingEnabled={true}
          showsHorizontalScrollIndicator={false}>
          <View style={styles.ScrollContainer}>
            <Text style={styles.ScrollTextContainer}>Screen 1</Text>
          </View>
          <View style={styles.ScrollContainer}>
            <Text style={styles.ScrollTextContainer}>Screen 2</Text>
          </View>
          <View style={styles.ScrollContainer}>
            <Text style={styles.ScrollTextContainer}>Screen 3</Text>
          </View>
          <View style={styles.ScrollContainer}>
            <Text style={styles.ScrollTextContainer}>Screen 4</Text>
          </View>
          <View style={styles.ScrollContainer}>
            <Text style={styles.ScrollTextContainer}>Screen 5</Text>
          </View>
        </ScrollView>
        <View style={styles.ButtonViewContainer}>
          <View style={styles.ButtonContainer}>
            <Button title="<" />
          </View>
          <View style={styles.ButtonContainer}>
            <Button title="1" />
          </View>
          <View style={styles.ButtonContainer}>
            <Button title="2" />
          </View>
          <View style={styles.ButtonContainer}>
            <Button title="3" />
          </View>
          <View style={styles.ButtonContainer}>
            <Button title="4" />
          </View>
          <View style={styles.ButtonContainer}>
            <Button title="5" />
          </View>
          <View style={styles.ButtonContainer}>
            <Button title=">" />
          </View>
        </View>
      </View>
      {/* </ScrollView> */}
    </View>
  );
}

const styles = StyleSheet.create({
  statusBar: {
    ...Platform.select({
      android: {
        height: 24,
      },
    }),
  },
  headerStyle: {
    flex: 1,
    textAlign: 'center',
    color: '#FFFFFF',
    fontFamily: Constants.app_font_family_regular,
    fontSize: 17,
    marginTop: 4,
  },
  paginatorHolder: {
    top: Constants.app_height - 120,
    backgroundColor: 'green',
  },
  center: {
    flexDirection: 'row',
    width: '50%',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: Constants.app_background_color,
  },
  right: {
    justifyContent: 'flex-end',
    marginRight: 20,
  },
  bottomLineStyle: {
    top: Constants.app_height - 125,
    height: 0.5,
    backgroundColor: Constants.app_grey_color,
  },
  noNetwork: {
    bottom: 0,
    textAlign: 'center',
    color: 'white',
    paddingTop: 5,
    paddingBottom: 5,
    backgroundColor: 'grey',
    ...Platform.select({
      ios: {
        fontSize: 19,
      },
      android: {
        fontSize: 16,
      },
    }),
  },
  scrollview: {
    ...Platform.select({
      ios: {
        marginTop: 1,
      },
      android: {
        marginTop: 0,
      },
    }),
  },
  appFontFamily: {
    fontFamily: Constants.app_font_family_regular,
  },
  backIcon: {
    height: 24,
    marginLeft: 5,
    width: 20,
    marginTop: 9,
    marginHorizontal: 10,
  },
  submitContainer: {
    top: Constants.app_height - 90,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: 0,
    padding: 0,
  },
  submitButton: {
    width: '20%',
    height: 30,
    marginBottom: 0,
    padding: 0,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    borderRadius: 3,
    backgroundColor: Constants.app_grey_color,
  },
  submitText: {
    color: 'black',
    fontSize: 12,
    fontFamily: Constants.app_font_family_regular,
    ...Platform.select({
      ios: {
        fontWeight: 'normal',
      },
    }),
    ...Platform.select({
      android: {
        justifyContent: 'center',
        marginBottom: 1.5,
      },
    }),
  },
  buttonContainer: {
    top: Constants.app_height - 120,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: 0,
    padding: 0,
  },
  button: {
    width: '100%',
    height: 40,
    marginBottom: 0,
    padding: 0,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    borderRadius: 3,
    backgroundColor: Constants.app_grey_color,
  },
  buttonText: {
    color: 'black',
    fontSize: 12,
    fontFamily: Constants.app_font_family_regular,
    ...Platform.select({
      ios: {
        fontWeight: 'normal',
      },
    }),
    ...Platform.select({
      android: {
        justifyContent: 'center',
        marginBottom: 1.5,
      },
    }),
  },
  MainContainer: {
    backgroundColor: Constants.app_background_color,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  ScrollContainer: {
    backgroundColor: Constants.app_background_color,
    flexGrow: 1,
    marginTop: 0,
    width: screenWidth,
    justifyContent: 'center',
    alignItems: 'center',
  },
  ScrollTextContainer: {
    fontSize: 20,
    padding: 15,
    color: '#000',
    textAlign: 'center',
  },
  ButtonViewContainer: {
    flexDirection: 'row',
    paddingTop: 5,
  },
  ButtonContainer: {
    padding: 10,
    width: '15%',
    // height: 20,
  },
});
